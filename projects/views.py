from django.shortcuts import render, get_object_or_404, redirect
from projects.models import Project
from projects.forms import CreateProjectForm
from django.contrib.auth.decorators import login_required


# Create your views here.
@login_required
def show_project(request):
    project_list = Project.objects.filter(owner=request.user)
    count = Project.objects.count()
    context = {
        "project_list": project_list,
        "count": count,
    }
    return render(request, "projects/list.html", context)


@login_required
def show_project_detail(request, id):
    project_detail = get_object_or_404(Project, id=id)
    context = {
        "project_detail": project_detail,
    }
    return render(request, "projects/detail.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = CreateProjectForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = CreateProjectForm()
    context = {"form": form}
    return render(request, "projects/create.html", context)
